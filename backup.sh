#!/bin/bash
sql_path=/home/ubuntu/backup/sqls
date=$(date +"%Y-%m-%d_%H-%M")
find $SQL_BACKUP_DIR -mtime +14 -type f -exec rm {} \;

for db in $(mysql $MYSQL_CONN --batch -N -e 'show databases')
  do
    if [ $db = 'information_schema' ]; then continue ; fi
    if [ $db = 'mysql' ]; then continue ; fi
    if [ $db = 'performance_schema' ]; then continue ; fi
    if [ $db = 'sys' ]; then continue ; fi

    mysqldump --lock-tables=false "$db" | gzip -c > "$sql_path/$date-$db.sql.gz"
    if [ $? -eq 0 ]
      then echo "$db dumped."
      else echo "$db dump failed."
    fi
done

