#!/bin/env bash
if [ "$1" = "" ]; then
    echo '첫 번째 인자값으로 서버 이름을 알려 주세요.'
    exit 1
fi

if [ "$2" = "" ]; then
    echo '두 번째 인자값으로 git에서 사용할 서버 이메일을 알려 주세요.'
    exit 1
fi

USER=$(whoami)

if [ "$USER" = "root" ]; then
    echo 'root 말고 일반 사용자로 실행해 주세요'
    exit 1
fi

SETUP_ROOT=$(realpath $(dirname $0))
touch "$SETUP_ROOT/setup.lock"

# 패키지 인스톨
if [ $(grep install "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo apt install software-properties-common && sudo add-apt-repository ppa:ondrej/php -y
	sudo apt update
	sudo apt upgrade -y
	sudo apt install -y apache2 libapache2-mod-security2 build-essential fail2ban mysql-server vim php8.1 php8.1-curl php8.1-fpm php8.1-gd php8.1-imagick php8.1-mbstring php8.1-mysql php8.1-sqlite3 php8.1-xml php8.1-zip
	sudo mysql_secure_installation
	echo install >> "$SETUP_ROOT/setup.lock"
else 
	echo "패키지 인스톨은 이미 됐습니다."
fi

# gitconfig
if [ $(grep gitconfig "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sed "s|server_name|$1|g" gitconfig | sed "s|server@email.com|$2|g" > $HOME/.gitconfig
	echo gitconfig >> "$SETUP_ROOT/setup.lock"
else
	echo "gitconfig 설정은 이미 됐습니다."
fi

# fail2ban
if [ $(grep fail2ban "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo cp jail.local /etc/fail2ban/
	sudo cp apache-404.conf apache-mytory.conf apache-wp.conf /etc/fail2ban/filter.d/
	sudo systemctl reload fail2ban
	echo fail2ban >> "$SETUP_ROOT/setup.lock"
else
	echo "fail2ban 설정은 이미 됐습니다."
fi

# composer
if [ $(grep composer "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php
	php -r "unlink('composer-setup.php');"
	sudo mv composer.phar /usr/local/bin/composer
	sudo chmod +x /usr/local/bin/composer
	sudo apt install zip unzip p7zip-full -y
	echo composer >> "$SETUP_ROOT/setup.lock"
else
	echo "composer를 이미 설치했습니다."
fi

# fail2ban-notification
if [ $(grep fail2ban-notification "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	cd $HOME
	git clone https://gitlab.com/mytory/fail2ban-notification.git
	cd $HOME/fail2ban-notification
	sed "s|server_name|$1|g" "$SETUP_ROOT/fail2ban-notification-env" > .env
	composer install
	echo "0 10    * * *   root    /usr/bin/php $HOME/fail2ban-notification/app.php > /dev/null 2>&1" | sudo tee -a /etc/crontab
	echo "fail2ban-notification" >> "$SETUP_ROOT/setup.lock"
else
	echo "fail2ban-notification을 이미 설치했습니다."
fi

# zsh
if [ $(grep zsh "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo apt install zsh -y
	sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
	echo "zsh" >> "$SETUP_ROOT/setup.lock"
else
	echo "zsh와 oh-my-zsh를 이미 설치했습니다."
fi

# wp-cli
if [ $(grep wp-cli "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
	chmod +x wp-cli.phar
	sudo mv wp-cli.phar /usr/local/bin/wp
	echo "wp-cli" >> "$SETUP_ROOT/setup.lock"
else 
	echo "wp-cli를 이미 설치했습니다."
fi

# backup-script
if [ $(grep backup "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	mkdir -p $HOME/backup/sqls
	cp $SETUP_ROOT/backup.sh $HOME/backup/
	sudo chmod +x $HOME/backup/backup.sh
	echo "0 1 	* * *	root	$HOME/backup/backup.sh" | sudo tee -a /etc/crontab
	echo "backup" >> "$SETUP_ROOT/setup.lock"
else
	echo "backup 스크립트를 이미 설치했습니다."
fi
	
# wp-tools
if [ $(grep wp-tools "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	cd $HOME
	git clone git@gitlab.com:mytory/wp-tools.git
	echo 'export PATH=$HOME/wp-tools:$PATH' >> $HOME/.zshrc
	echo "*/5 * 	* * *   $USER     $HOME/wp-tools/wp-cron.sh >/dev/null 4>&2" | sudo tee -a /etc/crontab
    echo "wp-tools" >> "$SETUP_ROOT/setup.lock"
else
    echo "wp-tools를 이미 설치했습니다."
fi


# timezone
if [ $(grep timezone "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo apt-get install --no-install-recommends -y tzdata
	sudo ln -fs /usr/share/zoneinfo/Asia/Seoul /etc/localtime && sudo dpkg-reconfigure -f noninteractive tzdata
	echo "timezone" >> "$SETUP_ROOT/setup.lock"
else
	echo "timezone을 이미 세팅했습니다."
fi

# locales
if [ $(grep locale "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo apt-get install --no-install-recommends -y locales
	sudo sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
	    sudo sed -i -e 's/# ko_KR.UTF-8 UTF-8/ko_KR.UTF-8 UTF-8/' /etc/locale.gen && \
	    sudo locale-gen
    echo "locale" >> "$SETUP_ROOT/setup.lock"
else
	echo "locale을 이미 세팅했습니다."	
fi

# setting php file upload size
if [ $(grep php-upload-size "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo sed -i 's|post_max_size = 8M|post_max_size = 100M|g' /etc/php/8.1/fpm/php.ini
	sudo sed -i 's|upload_max_filesize = 2M|upload_max_filesize = 100M|g' /etc/php/8.1/fpm/php.ini
	echo "php-upload-size" >> "$SETUP_ROOT/setup.lock"
else
	echo "php upload size 설정을 이미 했습니다."
fi

if [ $(grep php-fpm "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo apt install php-fpm libapache2-mod-fcgid
	sudo a2dismod php8.1
	sudo a2dismod mpm_prefork
	sudo a2enmod mpm_event
	sudo a2enconf php8.1-fpm
	sudo a2enmod proxy
	sudo a2enmod proxy_fcgi
	sudo apachectl configtest
	sudo systemctl restart apache2
	echo "php-fpm" >> "$SETUP_ROOT/setup.lock"
else
	echo "php fpm 설정을 이미 했습니다."
fi

# ufw log
if [ $(grep ufw-log "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo sed -i 's|#\& stop|\& stop|g' /etc/rsyslog.d/20-ufw.conf
	sudo systemctl restart rsyslog
	echo "ufw-log" >> "$SETUP_ROOT/setup.lock"
else
	echo "ufw log 설정을 이미 했습니다."
fi

# nodejs
if [ $(grep nodejs "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
	sudo apt-get install -y nodejs
	echo "nodejs" >> "$SETUP_ROOT/setup.lock"
else
	echo "nodejs 설치를 이미 했습니다."
fi


# wp ewww 플러그인용 이미지 최적화 프로그램들
if [ $(grep ewww "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo apt install -y gifsicle optipng pngquant libjpeg9 libwebp6
	EWWW_SRC_DIR=$(tempfile)
	rm $EWWW_SRC_DIR
	mkdir $EWWW_SRC_DIR
	cd $EWWW_SRC_DIR
	wget https://www.ijg.org/files/jpegsrc.v9e.tar.gz
	tar xzvf jpegsrc.v9e.tar.gz
	cd jpeg-9e
	sudo ./configure && sudo make && sudo make test && sudo make install
	echo "ewww" >> "$SETUP_ROOT/setup.lock"
else
	echo "ewww 설치를 이미 했습니다."
fi 

# mysql binlog 날짜 제한
if [ $(grep binlog_expire_logs_seconds "$SETUP_ROOT/setup.lock" | wc -l) = 0 ]; then
	sudo sed -i 's|# binlog_expire_logs_seconds	= 2592000|binlog_expire_logs_seconds = 172800|g' /etc/mysql/mysql.conf.d/mysqld.cnf
	sudo systemctl restart mysql
	echo "binlog_expire_logs_seconds" >> "$SETUP_ROOT/setup.lock"
else
	echo "binlog_expire_logs_seconds 설정을 이미 했습니다."
fi 

cd "$SETUP_ROOT"
